// 3 Inch Impeller
// ----------------------------------------------------------
// Joachim We, 2019-12-13
// ----------------------------------------------------------

use <libCopterMotors.scad>;
use <libCopterFCs.scad>
use <libCopterESCs.scad>
use <libCopterCams.scad>
use <libCopterTXs.scad>
use <libCopterFlyTowers.scad>;

// setup control
$fn=60;
fans = false;
top = false;
buttom = true;
stuff = false;
fan_stuff = false;

// prop profile
dp=3*25.6;
ba=.6*dp;
a=.4*ba;
b=.18*ba;

rs = 120;

// cassis
l = 1.2*dp/2;
xw = 6;      // x width
yw = 26/2;
offset_front=-40;    // fans mount front
offset_back=-40;     // fans mount front
h = .8;              // buttom mount cutoff



module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module copy_rotate(vec=[0,90,0])
{
    children();
    rotate(vec) children();
}

module screw(Mx=2, l=12, Head=4, Hexnut_l = 9){
    color("grey")cylinder(r=Mx/2, h=l);
    translate([0,0,-2])cylinder(r=Head/2, h=2);
    if (Hexnut_l > 0)
    {
        ra = Mx*2/sqrt(3);
        translate([0,0,Hexnut_l-1])cylinder(r = ra, h=3, $fn=6, center=true);
    }
        
}

module cutOff(){
    offset=30;
    for(i=[offset:120:360+offset])
    {
        rotate([0,0,i])
        union(){
            translate([dp/2+2.5,dp/2+15.5,rs])cube([dp, dp, dp/2+30], center=true);
            translate([-dp/2-2.5,dp/2+15.5,rs])cube([dp, dp, dp/2+30], center=true);
        }
        rotate([0,0,i+30])translate([dp-9, 0, rs])scale([1.6,1,1])cylinder(r=dp/2, h =50, center=true);
    }
}

module fan(mount=false){
    screw_pos = [29.5,0,7.2];
        
    if(mount == false){
    difference(){
        rotate([180,0,0])
        rotate_extrude(angle=360, convexity=2)
        rotate([0,0,100])translate([0,-(dp/2+b/2+1.2),00])
        polygon(points=[[-12.3,0.0],[-11.3,1.31],[-10.3,1.81],[-9.29,2.17],[-8.29,2.45],[-7.29,2.67],[-6.29,2.85],[-5.29,2.99],[-4.29,3.11],[-3.29,3.2],[-2.29,3.26],[-1.29,3.3],[-0.288,3.32],[0.712,3.31],[1.71,3.27],[2.71,3.21],[3.71,3.13],[4.71,3.02],[5.71,2.88],[6.71,2.73],[7.71,2.56],[8.71,2.37],[9.71,2.17],[10.7,1.96],[11.7,1.73],[12.7,1.49],[13.7,1.24],[17.7,.9],[17.7,-0.00488],[16.7,-0.0268],[15.7,-0.0645],[14.7,-0.116],[13.7,-0.179],[12.7,-0.251],[11.7,-0.331],[10.7,-0.416],[9.71,-0.505],[8.71,-0.594],[7.71,-0.683],[6.71,-0.768],[5.71,-0.848],[4.71,-0.921],[3.71,-0.985],[2.71,-1.04],[1.71,-1.08],[0.712,-1.1],[-0.288,-1.11],[-1.29,-1.1],[-2.29,-1.09],[-3.29,-1.07],[-4.29,-1.04],[-5.29,-0.998],[-6.29,-0.95],[-7.29,-0.89],[-8.29,-0.816],[-9.29,-0.724],[-10.3,-0.605],[-11.3,-0.437],[-12.3,0.0],[-12.3,0.0]]);
        translate([dp/2,0,1.2])rotate([0,-80,0])scale([.7,1.5,1])cylinder(d=5, h=20, center=true);
        translate([0,0,-25])cube([1.2*dp, 1.2*dp , 5], center=true); // sauberer Bodenabschluss
        
    }
   
    difference(){
        union(){
        intersection(){        
            translate([0,0,-rs+13.7])
        
            difference(){
                // motor mount
                sphere(r=rs);
                sphere(r=rs-3);
                translate([0,0,-50])cube([2*rs,2*rs,2*rs], center = true);
                cutOff();
                
            }      
            cylinder(d=1.2*dp-3, h= 80, center=true);
            
        }
        
        translate([0,0,11.5])cylinder(r=8.8,h=3,center=true);
        //translate([20,0,10])scale([1.5,0.5,1])cylinder(r=2, h = 3.5);
        //translate([35,0,7])scale([2,0.5,1])cylinder(r=2, h = 3.5);        
        }
        translate([0,0,10])rotate([180,0,0])intersection(){
            motor1306(true);
            cylinder(r=dp/3, h = 50, center=true);
        }
       
        translate(screw_pos)screw();
    }
}
    
    if (mount){
        color("blue") difference(){
            intersection(){
                rmount= 75;//68;
                alpha = -0;// 2*PI*(60)/360;
                //echo(alpha);
                translate([cos(alpha)*rmount,sin(alpha)*rmount,-230])sphere(r=250);
                translate([cos(alpha)*rmount/2,sin(alpha)*rmount/2,-2]) cube([rmount,rmount,40], center=true);
                translate([cos(alpha)*rmount,sin(alpha)*rmount,0.5]) cylinder(r=50, h=36);
            }
            translate([0,0,-100])cutOff(); 
            translate([0,0,-rs+13.7])     sphere(r=rs);
           
            //translate([20,0,10])scale([1.6,0.6,1.2])cylinder(r=2, h = 3.5);
            //translate([35,0,6])scale([2.1,0.6,1.1])cylinder(r=2, h = 3.5);                    
            translate(screw_pos)screw();               
          
            translate([55,5,11])rotate([90,0,0])cylinder(r=5.5,h=10);
            translate([42,5,12])rotate([90,0,0])cylinder(r=3.5,h=10);       
            translate([47,5,7])rotate([90,0,0])cylinder(r=1.5,h=10);          
        }
    } 
    
    
    if (fan_stuff)
        translate([0,0,10])rotate([180,0,-30]) motor1306(true);

}

module fans(nomount){         
    translate([l+yw,l+xw,0])rotate([0,0,180-offset_back])fan(nomount);
    translate([-(l+yw),l+xw,0])rotate([0,0,+offset_back])fan(nomount);
    translate([l+yw,-(l+xw),0])rotate([0,0,180+offset_front])fan(nomount);
    translate([-(l+yw),-(l+xw),0])rotate([0,0,-offset_front])fan(nomount);    
}

module CONNECTOR(ws=5, we=8, thickness = 3, height=20){    
        
    for(i = [0:1:thickness])
    {
        translate([i,0,0])
        cube([1, ws+((we-ws)/thickness)*i, height],center = true);       
    }
}

module STUFF(exp=true, cam=true, clr=0){
  
    // FC/ESC  
    IFlighSucceX(exp,clr);
    
    // RX 
    translate([0,0,15])rotate([180,0,0])RX_XMPLUS(false);
    
    // Beeper
    if (exp)
        translate([0,19,2])rotate([-90,0,0])BEEPER();    
    else
        translate([0,15,2])rotate([-90,0,0])BEEPER();    
    
    // CAM
    if (cam)
        translate([0,-20,6])rotate([55,0,0])RUNCAM_NANO(exp);
}

module core(){
    w=10;
    d=16;
    difference(){
        union(){
            minkowski(){
                hull(){ STUFF(false); }
                sphere(r=1.5);
            }
            translate([0,23,2])rotate([90,0,0])cylinder(d=12, h = 2.5);            
            // Antenna Mount VTX
            translate([0,14,10])rotate([0,0,180])AntHolder();
            // RX Antenna support
            copy_mirror([1,0,0])translate([10,9,14])rotate([-20,45,0])cylinder(d=5.5, h = 5,center=true);
        }
        hull() { STUFF(false,false); }
        STUFF(true,true);
        translate([0,10,15])rotate([-40,0,0])scale([1,.85,1])cylinder(d=17, h = 19.5, center = true);
        // rx antenna
        copy_mirror([1,0,0])translate([10,9,14])rotate([-20,45,0])cylinder(d=3, h = 5.5,center=true);
        
        // motor cable holes
        copy_mirror()translate([0,-5,-7])rotate([0,90,0])
        hull(){
            cylinder(d=3, h = 40, center =true);
            translate([0,-4,0])cylinder(d=3, h = 40, center =true);
        }
        
        // power cable
        translate([0,10,-7])rotate([90,90,0])
         hull(){
            translate([0,2,0])cylinder(d=4, h = 40, center =true);
            translate([0,-2,0])cylinder(d=4, h = 40, center =true);
        }
    }
}

module complete_core(){
    difference(){
        fans(true);
        translate([0,0,10]) hull(){ STUFF(false); }
        translate([0,0,10])STUFF(true);
    }
    translate([0,0,10])core();
}


module connect(){
    for(i=[45:90:360])
        rotate([0,0,i])translate([22,0,.5])screw(Mx=2, l=6, Head=4, Hexnut_l = 6.5);
}

/** Antenna Section - author is not satisfied */

module AntHolderBolts()
{    
    union()
    {
        copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.5,0,0])  cylinder(h=5,d=1.75); 
        copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.5,0,5])  cylinder(h=7,d=2.1);
        copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.5,0,12])  cylinder(h=25,d=3.8);
    }
}

module AntHolder()
{
    difference()
    {   
        translate([0,0,20])
        difference(){
            rotate([0,-90,0])
                rotate_extrude(angle=60,$fn=60)
                translate ([-20,0]) difference()
                {
                    circle(4);
                    translate([2,0]) circle(1.25);
                    translate([7,0]) square(10,center=true);
                }
          
        }
        //translate([0,5,0]) AntHolderBolts();
    
        translate([0,-14,0]) copy_mirror([1,0,0]) rotate([-30,0,0]) translate([3.6,0,0]) cylinder(h=7,d=4); 
        rotate([30,0,0])translate([0,-25,4])  cube(28,center=true);
    }
}

module AntMount()
{
    difference(){
        translate([0,0,0])
        hull(){
            translate([0,0,-1])cube([8,2,6], center=true);
            translate([5,4,-4])cylinder(d=2, h=6);
            translate([-5,4,-4])cylinder(d=2, h=6);
        }
        translate([0,7,0]) AntHolderBolts();
    }
}


if (fans)
{
    translate([0,0,-10])
    fans(false);
}
if (top)
{
    difference(){       
        complete_core();        
        
        translate([0,0,0+h])cube([70,70,4], center=true);
        connect();   
    }       
}

if (buttom)
{
    translate([0,0,-4])
    difference(){
        union(){
            complete_core();            
            minkowski(){
                cube([23,23,5],center = true);
                sphere(d=2);
            }

        }
        translate([0,0,17+h])cube([110,110,30], center=true);
        translate([0,0,10])STUFF();
        
        // motor cable holes
        copy_mirror()translate([0,-5,3])rotate([0,90,0])
        hull(){
            cylinder(d=3, h = 40, center =true);
            translate([0,-4,0])cylinder(d=3, h = 40, center =true);
        }
        
     #   translate([0,0,-1])cube([30,11,2],center=true);
        translate([0,0,-1])rotate([0,0,90])cube([30,10,2],center=true);
        translate([0,0,-5])cylinder(d=11,h=10);
        connect();     
    }   
    
}

if (stuff)
    translate([0,0,10])STUFF(exp=true, cam=true, clr=1);
