
pi_heigth = 0.5;

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

// V 1.3
module picam(clr=false, cable=false){
    eps = clr ? 1 : 0.;
    difference(){
       cube([25+eps,24+eps,1],center=true);
        if (clr==false) {
        for(i=[2,14.5])
            for(j=[21/2,-21/2])
                translate([j,-25/2+i,-5])
                    cylinder(r=1,$fn=20,h=10);
        }
    }
    translate([0,-1,3]) cube([9+eps,9+eps,5],center=true);
    
    // cable
    if (cable)
    {
        color("red")translate([0,13,-1.75])cube([17,5,2.5],center=true);
    }
    
}

module front(){
    $fn = 40;
    
    
    difference() {
       union(){
           minkowski(){
              cube([25,24,5], center=true);
              sphere(r=2);
           }
            translate([0,-15,0])rotate([0,0,90]) snap();
        }
       for(dz=[-4:1:3])  
            translate([0,0,dz]) picam(true, false);
       for(i=[2,14.5])
         for(j=[21/2,-21/2])
            translate([j,-25/2+i,1.5]) cylinder(r=.8,$fn=20,h=2.8);
       for(dz=[-4:1:pi_heigth ])  
            translate([0,0,dz]) picam(true, true);
    }
    color("blue")for(i=[2,14.5])
       for(j=[21/2,-21/2])
          translate([j,-25/2+i,1.5]) 
            difference(){ cylinder(r=1.7,$fn=20,h=2); cylinder(r=.8,$fn=20,h=10);} 
            
    
         
   
}

module back() {
    eps = .5;
    cube([25+eps,24+eps,2],center=true);
    for(i=[-(25+eps-1)/2,(25+eps-1)/2]){
        translate([i,-2,2])cube([1,4,2.5], center=true);
    }
    //translate([0,(24+eps-2)/2,1])cube([17,2,1], center = true);
    translate([0,-(24+eps-2)/2,2])cube([4,2,2.5], center = true);
}

module snap(d_sphere=13.)
{
    difference(){
    copy_mirror()
    translate([0,6,0]) rotate([90,0,0])
    minkowski() {
        cube([10,1,2], center=true);
        cylinder(r=4, h=1, $fn=20, center=true);
    }
    
    translate([-4,0,0])sphere(d=d_sphere, $fn=40,center=true);
    cube([20,10,20], center=true);
    translate([9,0,0])cube([12,20,20], center=true);
    }
}
   



if(true)
{    
  // front();
   //color("grey") translate([0,0,pi_heigth ]) picam(false, true);
    color("green") translate([0,0,-3.5]) back();
  // 
}
else
{
difference(){
  front();
  translate([0,0,5])cube([30,30,3], center=true);
}
translate([0,0,pi_heigth ])picam();
}
