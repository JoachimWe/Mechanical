/*
Copter parts FlyTowers library

(c) 2018 Joachim Weishaupt
https://gitlab.com/joachimwe

*/ 

$fn = 80;

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

// SUPER_S Flytower with 6A ESC
module SUPER_S(clr=false, mount=true){   

    //cube([25,25,3],true);                           // ESC
    // ESC
    translate([-23/2,-23/2,-1.5])
    minkowski()
    {
      cube([23,23,2]);
      cylinder(r=2,h=1);
    }
    // Motor Plugs
    dist = 1.77;
    for(i = [0:5])
    {        
        color("red") translate([-25/2-2,-5*dist+i*2*dist,0]) cube([2,dist,2], true);
        color("red") translate([25/2+2,-5*dist+i*2*dist,0]) cube([2,dist,2], true);
    }
    
    
    if (clr) {
        color("grey") translate([0,0,4])cube([20,20,5],true);    // inbetween
        color("red") translate([9,0,9.5])cube([6,8,2.2], true);   // USB
        color("Salmon") translate([32,0,8.5])
        minkowski(){
            cube([40,11,6], true);    // USB free for plug
            sphere(d=1.7);
        }
    }
    translate([0,0,8.5])cube([20,20,4],true);        // FC

    h_mount = 19;
    d_mount = 2.2;   
    copy_mirror([1,0,0]) copy_mirror([0,1,0]) translate([8,8,-h_mount/2+1])
       cylinder(h_mount,d=d_mount,true);
           
}

// IFlight SucceX V1 Mini Flug Turm 2-6S mit SucceX F4
module IFlighSucceX(clr=false, e = 0)
{
    
    //color("green")translate( [0,0,.5])cube([23,23,21],center=true);
    minkowski(){
        cube([22+e,19+e,18+e],center=true);
        cylinder(d=5,h=1);
    }

    
    if (clr) {
        
        //color("red") translate([9,0,9.5])cube([6,8,2.2], true);   // USB
        color("Salmon") translate([32,2.5,2])
        minkowski(){
            cube([40,11,6], true);    // USB free for plug
            sphere(d=1.7);
        }
        
        // VTX Antenna
        color("blue")translate([0,0,19/2+3])rotate([-90,0,0])cylinder(d=3, h =19);
        
        translate([8,8,0])color("red")M2screw();
        translate([-8,8,0])color("red")M2screw();
        translate([8,-8,0])color("red")M2screw();
        translate([-8,-8,0])color("red")M2screw();     
    }
}

module M2screw(){
    cylinder(d=1.6, h=23, center = true);
    translate([0,0,-13]) cylinder(d=4, h=4, center = true);
}



// Power BECs
module VCC33(clr=false)
{
    cube([8,9,5], true);
    if (clr){
        for(i = [-1:1]){
            color("red") translate([2*i,7.5,3])rotate([90,0,0])cylinder(5,r=.75,true);
             color("red") translate([2*i,-2.5,3])rotate([90,0,0])cylinder(5,r=.75,true);
        }     
    }
}

SUPER_S(true);
translate([0,40,0])IFlighSucceX(true);
