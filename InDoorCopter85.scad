/*
Indoor Copter
1103 motors

(c) 2017 Fabian Huslik
(c) 2018 Joachim Weishaupt

https://github.com/fabianhu/
*/ 

$fn = 60;
//radius = 2 * (25.4/2)+0.5; // propeller radius
radius = (51.6/2)+.5; // propeller radius          2045 Zoll
//radius = (59/2)+0.5; // propeller radius              2345 Zoll
//radius =(63.5/2)+0.5; // propeller radius           2540 Zoll
echo("Prop radius: ",radius);
thick = .8; // rest body thickness
fthick = 1.2; // fin thickness
trad = 3.0; // intake aerodynamic radius
height = 12; //height of tunnel

dx=radius*2+2*thick+13+2+4;
dy=radius*2+2*trad-2*thick+0.3+7+2+13;   // +2

motor_offset = -30;
camAngle = 30;

ang = 20; // fin angle
dlt=2.1; // fin height

use <libCopterCams.scad>
use <libCopterESCs.scad>
use <libCopterFCs.scad>
use <libCopterFlyTowers.scad>
use <libCopterMotors.scad>
use <libCopterTXs.scad>;

// helper to mirror while keeping the original.
module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module completeRing(rot=0,zer=17){
rdiv =6;
    difference()
    {
        union()
        {
            // outer cylinder
            translate([0,0,-height])
            difference()
            {
                translate([0,0,height])torus();
                *translate([0,-radius,4-3]) rotate([90,0,zer]) cylinder(d=3,h=3,center=true); // cable bore
                translate([0,0,4-3]) rotate([90,0,zer]) translate([0,0,radius])cylinder(d=3,h=5,center=true); // cable bore
            }

            translate([0,0,-height+cos(ang)*dlt+thick/2-0.3]) 
            for(i=[0:360/rdiv:359])
            {
                rotate([0,0,i+zer]) translate([rot>0?4:-4,0,0]) rotate([90,90-rot,0]) Fin();
            }
            translate([0,0,-height]) cylinder(d=15+thick*2,h=5); // motor carrier (bell diameter from motor)
            
        }
        translate([0,0,-height+thick]) rotate([0,0,zer-90]) motor1103(true);
        
    }
}


module Fin(){
    hull()    
    {
        translate([dlt,0,0]) cylinder(d=fthick,h=radius);
        translate([-dlt,0,0]) cylinder(d=fthick,h=radius);
    }
} 
module torus() // might not work for every thickness and trad.
{
    topdia = thick+0.7+0.5;
    
    rotate_extrude(convexity = 5 )
    translate([radius+trad, 0, 0])
    hull() // roundness at top
    {
        translate([-trad,-height])square([thick,height]);
        circle(r = trad);
    }
}



//FCpos= [-3,13+6,-3.2-height + 14];
FCpos= [-3,13+7,-3.2-height + 10];  // SUPER_S
RXPos = [23,5,-5-(height-14)];      // RX XM
VCCPos = [0,22,-height+2 ];

campos=[0,48,-5.3+3];
//campos=[0,44.5,-5.3+5];
//campos=[0,37,-5.3+3];

module STUFF(exp=false, zer = 17)
{
    translate(campos) rotate([-90+camAngle,0,0]) 
    union()
    {
        //CAMERA(exp);
        rotate([0,0,180])FPVCamTX(false);
        //if(exp)
        translate([0,0,-4]) cube([15,13,35],center=true); // room to install cam
    }
    dwn = height - 14;
        
    if(true) // star - stack config
    {
        //translate(RXPos) rotate([-90,0,-21]) RX_XM(exp);
        translate(VCCPos) rotate([0,0,90]) VCC33(exp); 
    }
    else
    {
        translate([0,0,-11-dwn])   rotate([0,0,0]) ESC16x16(exp);
        translate([0,0,-6.5-dwn]) rotate([0,0,180]) ESC16x16(exp);
        translate([0,0,-2-dwn])  rotate([0,0,180]) REVO16x16(exp);
        translate([0,0,2.5-dwn]) rotate([0,0,0]) RX_XM(exp); 
        
    }
       
     if(exp)
     {   
        copy_mirror([1,0,0]) copy_mirror([0,1,0]) translate([dx/2,dy/2,-height+thick*2]) rotate([0,0,-90+zer]) motor1103(exp);
     } 
     
     if(exp)    // Flytower
     {
         translate([0,3,-8])rotate([0,0,180])SUPER_S(true);
     }
   
}

BattSize=[13,42,17.5];
//BattSize=[14.5,30,20];
//BattPos = [0,-19,-height+17.5/2+thick]; // BattSize z !
//BattPos = [0,-35,-height+18.5/2]; // BattSize z !
BattPos = [0,-27,height-1.5];   // vertical

module Battery()
{
    BattRad=1.5;  
    minkowski(){
        cube(BattSize-[BattRad,BattRad,BattRad],center=true);
        sphere(BattRad/2);
    }    
}

module BattHld()
{
    height=25;
    difference()
    {
        minkowski(){
            Battery();
            sphere(thick+0.2,center=true);
        }
        Battery();
        translate([0,-21,-0.5]) cube([15,2,21],center=true); // minus end
             
        translate([0,15,0]) cylinder(d=10,h=height,center=true);        
        translate([0,1,0])cylinder(d=10,h=height,center=true);
        translate([0,-13,0])cylinder(d=10,h=height,center=true);        
        
        translate([0,15,0]) rotate ([90,0,0]) cylinder(d=11,h=height,center=true);
       
        translate([0,12,0])rotate ([90,0,90])cylinder(d=13,h=height,center=true); 
        translate([0,-12,0])rotate ([90,0,90])cylinder(d=13,h=height,center=true); 
    }   
}

module body()
{
    battAngle = -25;
    // 4 fans
    difference()
    {
        copy_mirror([1,0,0]) copy_mirror([0,1,0]) translate([dx/2,dy/2,0]) completeRing(ang,motor_offset);
        translate(BattPos) rotate([-90,battAngle,90]) Battery();
        translate([0,30,-5])cube([17,30,20],true);
        copy_mirror([1,0,0]) translate([18,8.5,-5])rotate([0,0,-35])cube([32,3,20], true);
        copy_mirror([1,0,0]) translate([12,12.5,-5])rotate([0,0,-65])cube([30,6,20], true);
        
    }
    // outer side connection
    copy_mirror([1,0,0]) translate([dx/1.8,0,-height/2]) 
        cube([1.5*thick,trad*1.8+22,height],center=true);    


    translate(BattPos) rotate([-90,battAngle,90]) BattHld();
    
    // batt side support
    *copy_mirror([1,0,0]) union() 
    {
        translate([-16+2.6,-1-14,height/2-12])
        difference(){
            cube([14,thick,height-2+2.5],true);
            translate([0,5,0])rotate([90,0,0])cylinder(h=10,d=8);
        }
        translate([-16+6.5,-1-14-19.8,height/2-12])
                    cube([6,thick,height-2+2.5],true);
            
        
    }

    //translate([-.5,-.5,height-7]) translate(RXPos) 
    //    rotate([-90,0,-21]) cube([6.5,4.5,4.5],true);    // Rx Support top
    
    // top
    difference()
    {
        union()
        {         
            minkowski()
            {
                innertop();
                cube(thick*4,center=true);
            }
            //translate([0,34,0]) cylinder(h=10,d=16); // TX antenna supp        
        }
        innertop(); 
        cube([100,100,5],center=true);
        
        copy_mirror([1,0,0]) copy_mirror([0,1,0]) translate([dx/2,dy/2,-height-1])   cylinder(r=radius,h=2); // clean out main bores
        
        translate([0,-1,-height]) cylinder(h=30,d=20); // rear center
       
        copy_mirror([1,0,0]) hull(){
            translate([18,5,-height]) cylinder(h=30,d=8); // front center
            translate([18,-5,-height]) cylinder(h=30,d=8); // front center
        }       
        
        translate([0,34,-height]) cylinder(h=4+31-4,d=14); // TX antenna hole
        translate([0,34,-height+31-10]) cylinder(h=2,d1=14,d2=16); // TX antenna hole
        
        //translate(BattPos) Battery();
                   
        copy_mirror([1,0,0])translate([10.5,13,-height])  cylinder(h=30,d=9);      
      
        // Buzzer
        // 12x9mm 
        translate([0,19,-height]) cylinder(h=30,d=12); // front center  
    }
    
    translate([0,19,height-7.5]) difference() 
    {  cylinder(h=4,d=14.5); 
       translate([0,0,-2])cylinder(h=7,d=12);
    }    
    
    // cam holder
    difference(){
        translate(campos) rotate([-90+camAngle,0,0]) 
        difference()
        {
           union()
           {
               translate([0,0,-4]) cube([16.5,16.0,4],true);         
               translate([0,6.0,-6]) rotate([0,90,0]) cylinder(d=5,h=21.5,center=true);           
                   
           }
           CAMERA(true);      
           
        }
        translate([21.3,40.0,-9])rotate([0,0,61])cube([20, 20, 10], true);
        translate([-21.3,40.0,-9])rotate([0,0,-61])cube([20, 20, 10], true);
    }
    
    // reinforcements for bolts from bottom cover
    ItfPlace() cylinder(d=3.5,h=4);
    

}

// pacement module for interface to lower cover.
module ItfPlace()
{
    copy_mirror([1,0,0]) translate([10,30.5,-height]) children();
    //copy_mirror([1,0,0]) translate([36.5,1.5,-height]) children();
    copy_mirror([1,0,0]) translate([dx/1.8-1.2,1.5,-height]) children();    
    copy_mirror([1,0,0]) translate([26.5,-13.5,-height]) children(); 
}

module fc_mount(){
    // FC    
    translate([0,0,0])
    copy_mirror([1,0,0]) copy_mirror([0,1,0])  translate([8,8,-height]) children();
}

module innertop()
{
     translate([0,0,2.5]) 
    difference()
    {
        linear_extrude(height = 3,scale = 0.9)
        {
           polygon([[-8,43],[8,43],[30,20],[30,-15],[-30,-15],[-30,20]]);
        }
        copy_mirror([0,1,0])
        copy_mirror([1,0,0]) translate([dx/2,dy/2,0]) cylinder(r=radius+trad-thick+1,h=20);
        translate([0,-20,-height]) cylinder(d=12,h=30); //free batt hole
    }
    
}


module bottombolt()
{
        translate([0,0,-3-thick]) cylinder(d=4,h=3);
        translate([0,0,-thick]) cylinder(d=2.2,h=thick);
        translate([0,0,0]) cylinder(d=1.75,h=6);
}

module bottom_old()
{
     
    difference()
    {
        translate([0,0,-height-thick]) linear_extrude(height = thick,scale = 1.0)
        {
        polygon([[-8,41],[8,41],[38,+3],[38,-3],[-38,-3],[-38,+3]]);
        }
        copy_mirror([1,0,0]) translate([dx/2,dy/2,-0.5-height-thick]) cylinder(r=radius+thick,h=20);
        translate([0,-4,-height]) cylinder(d=12,h=30); //free batt hole
        ItfPlace() bottombolt();
    }
    
}

module innerbottom()
{
    difference()
        {
            polygon([[-8,44],[8,44],[dx/1.8,+3.5],[dx/1.8,-2.5],[dx/1.8-10,-15],[-dx/1.8+10,-15],[-dx/1.8,-2.5],[-dx/1.8,+3.5]]);
            copy_mirror([1,0]) translate([dx/2,dy/2])  circle(r=radius);
            copy_mirror([1,0]) translate([dx/2,-dy/2])  circle(r=radius);
            translate([0,3]) circle(d=14); //free batt hole
        }
}

module bottom()
{    
    difference()
    {
        union()
        {
            translate([0,0,-height-thick]) linear_extrude(height = thick*2.5,scale = 1.0)
            {
                innerbottom();
            }
        } 

        translate([0,0,-height]) linear_extrude(height = thick+1,scale = 1.0)
        {
            offset(r=-2) innerbottom();
        }
 
        ItfPlace() bottombolt();
        translate([0,3,1.8]) fc_mount() bottombolt(); 
              
        copy_mirror([1,0,0]) translate([15,-2,-height-1.5]) cylinder(d=10.5,h=3);
        copy_mirror([1,0,0]) hull(){
            translate([27,0,-height-1.5]) cylinder(d=10,h=3);
            translate([dx/1.8-10,0,-height-1.5]) cylinder(d=10,h=3);
        }
        copy_mirror([1,0,0])
        hull(){
         translate([16,8,-height-1.5]) cylinder(d=5,h=3);
         translate([11,14,-height-1.5]) cylinder(d=4,h=3);
        }
        translate([0,19,-height-1.5]) cylinder(d=12,h=3);
        translate([0,34,-height-1.5]) cylinder(d=12,h=3);
        translate([0,-25,-height-1.5]) cylinder(d=32,h=3);
        
    }
    
    // RX 
    /*
    difference(){
        translate([-1.5,-0.5, -7.5]) translate(RXPos) rotate([-90,0,-22]) cube([3.5,4.5,4.5],true);    // Rx Support top   
        translate([-1,0,-7])translate(RXPos) rotate([-90,0,-26]) RX_XM(false);
    }       
    */
              

    translate([0,3,-.5])
    difference()
    {
         fc_mount() cylinder(h=2.5, d=4);
         fc_mount() bottombolt(); 
    }
}
/*
if(false){
    body();   
    STUFF(true,motor_offset); 
    ItfPlace() bottombolt(); 
  //  bottom();
}
else{
    difference()
    {
        body();
        color("grey") STUFF(true,motor_offset);   
        ItfPlace() bottombolt();
        color("red")translate([0,0,-height-5+0.02]) cube([150,150,10],center=true); // test limitation for a even bottom
        color("red")translate([0,0,+height+30]) cube([150,150,40],center=true); // test limitation for a even upper
    } 
}
*/
//translate([100,0,10]) 
bottom(); // elevate Z to make cleat that the stl needs to be split in Slic3r



