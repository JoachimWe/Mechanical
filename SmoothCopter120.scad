// 120 Frame for 1306 3 inch props
// 20mm Controll-Stack

demo = true;        // true demo
$fn = 20;

d = 3.0 * 26.5 + 0.25;
l = sqrt(d*d/2)+2.5;
echo(l);
h = 3;
motor_mount = 15;

cs_l = 25;

use <libCopterMotors.scad>

module mount(){
    minkowski(){
        union(){
        cube([2*l, 4, h], center = true);
        translate([l,0,0])rotate([0,0,180])cylinder(d=motor_mount,h = h, center=true);
        translate([-l,0,0])cylinder(d=motor_mount,h = h, center=true);
        }
        cylinder(r=1.5,h=1);
    }    
}


if(demo){
    // arms
    mount();
    translate([l,0,1.5]) rotate([0,0,180]) motor1306(true);
    translate([-(l),0,1.5])  motor1306(true);
    rotate([0,0,90]){
        mount();
        translate([l,0,1.5]) rotate([0,0,180]) motor1306(true);
        translate([-(l),0,1.5]) motor1306(true);
    }
    
    // control-stack (cs)
    rotate([0,0,45])translate([0,0,0.5])cube([cs_l,cs_l,h+1], center = true);
    
}
else
{
     // arms
    difference(){
        mount();
        translate([l,0,1.5]) rotate([0,0,180]) motor1306(true);
        translate([-(l),0,1.5]) motor1306(true);
    }
    rotate([0,0,90]){
        difference(){
            mount();
            translate([l,0,1.5]) rotate([0,0,180]) motor1306(true);
            translate([-(l),0,1.5]) motor1306(true);
        }
    }
    
    // control-stack (cs)
    rotate([0,0,45])translate([0,0,0.5])cube([cs_l,cs_l,h+1], center = true);
}
