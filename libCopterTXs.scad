/*
Copter parts TX library

(c) 2018 Fabian Huslik

https://github.com/fabianhu/

extended (c) 2018 Joachim Weishaupt
https://gitlab.com/joachimwe

*/ 

translate([0,0,0]) rotate ([40,0,0]) TX03();
translate([50,0,0]) TX_MM213TL();

translate([0,40,0]) rotate ([40,0,0]) TX03(true);
translate([50,40,0]) TX_MM213TL(true);

module TX03(clr=false)
{
  color("green") cube([19.6,17,4],center=true); // base PCA
  color("grey") translate([1,11.3,1.5]) cube([6.5,6,5],center=true);  // antenna PCA
  color("grey") translate([-6,2.5,2]) cube([7.6,10,3],center=true);  // LED
  color("grey") translate([-6.3,7,-1]) cube([5,4,2],center=true);  // SW
  color("grey") translate([1,14,2]) rotate([-90,0,0]) cylinder(d=2.5,h=54,$fn=12);
  color("grey") translate([1,14+30,2]) rotate([-90,0,0]) cylinder(d=5,h=10);
  color("grey") translate([6,-7,1.5]) cube([7,4,3],center=true);  // cabling
  if(clr)
  {
       color("red") translate([-6.5,9,-1]) rotate([-90,0,0]) cylinder(d=3,h=30); // actuation of switch
       color("red") translate([6,-7,1.5]) cube([7,4,10],center=true);  // cabling
       color("red") cube([19.6+1,17+1,4+1],center=true); // base PCA
  }
}


module TX_MM213TL(clr=false)
{
	color("grey") cube([19.5,22.5,3.5],true);

	color("grey") translate([5,-10.5,0]) rotate ([0,90,0])  cylinder(d=4,h=27);
	color("red") if(clr)
        {
		translate([0,10,0.5]) cube([13,4,4],true);
	}
}


module FPVCamTX(clr=false)
{   
   h_lens=6.7; 
   insert_len = 5;  
   
   echo("Antenna space", insert_len);    
   
   color("grey") translate([0.75,0,1]) union(){    
       cube([8,8,2],true);
       translate([0,0,1])cylinder(5, r=4);
       translate([0,0,6])cylinder(4, r=5);
   }
   color("grey") translate([0,0,-4.5])cube([14.5,12,9],true);
   color("grey") translate([-8/2, 7.9,-(1.5/2)-6.5+.75-insert_len/2])cube([6.5,3.8,insert_len],true);
   color("grey")translate([1.8,4,-9]) cube([5,3,2]);    // button
  
   color("red") if (clr)     // Antenna
   {
       translate([-4.5,15,-8.5])rotate([90,0,0])cylinder(10,r=1);
       translate([-4.5,40,-8.5])rotate([90,0,0])cylinder(25,r=2.5);
       translate([0,0,h_lens]) CAMANGLE(120); // view angle
   }
   
   
}

